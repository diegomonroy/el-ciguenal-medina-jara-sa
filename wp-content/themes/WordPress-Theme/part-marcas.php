<!-- Begin Marcas -->
	<section class="marcas" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'marcas' ); ?>
			</div>
		</div>
	</section>
<!-- End Marcas -->